package domain;
public class Voice extends Message {

	public void setPayload(String text) {
		this.payload = text;
	}

	public String getPayload() {
		return payload;
	}

	String payload;
}
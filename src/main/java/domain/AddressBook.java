package domain;
import java.util.HashMap;

public class AddressBook {

	private HashMap<String,Mailbox> content;

	public AddressBook() {
		content = new HashMap<>();
	}

	public Mailbox getMailbox(String address) {
		return (Mailbox) content.get(address.toLowerCase());
	}

	public void addMailbox(Mailbox mbx) {
		content.put(mbx.getAddress().toLowerCase(), mbx);
	}

}
package ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Console {
	BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

	public String readLine(String prompt) {
		String line = null;
		System.out.println(prompt);
		try {
			line = reader.readLine();
		} catch (IOException e) {
		}
		return line;
	}

	public void show(String str) {
		System.out.println(str);
	}
}
package ui;

import domain.AddressBook;
import domain.Mailbox;

public class DeleteMessageInteraction {
	private Console console;
	private AddressBook addressBook;

	public DeleteMessageInteraction(Console console, AddressBook addressBook) {
		super();
		this.console = console;
		this.addressBook = addressBook;
	}

	public void deleteMessage() {
		String mbxAddress = console.readLine("Delete message from Mailbox:");
		Mailbox mbx = addressBook.getMailbox(mbxAddress.toLowerCase());

		if (mbx == null) {
			console.show(mbxAddress + " is not a valid address !");
			return;
		}

		try {
			Integer messageId = new Integer(console.readLine("Enter message id:"));
			mbx.delete(messageId.intValue() - 1);
		} catch (Exception e) {
			console.show("invalid message id !");
		}
	}
}
package ui;

public class Desktop {

	private ListMailboxInteraction listMailboxUIAction;
	private SendMessageInteraction sendMessageUIAction;
	private DeleteMessageInteraction deleteMessageUIAction;

	private Application application;
	private Console console = new Console();

	public Desktop(Application app) {
		this.application = app;
		listMailboxUIAction = new ListMailboxInteraction(console, app.getAddressBook());
		sendMessageUIAction = new SendMessageInteraction(console, app.getAddressBook());
		deleteMessageUIAction = new DeleteMessageInteraction(console, app.getAddressBook());
	}

	class SendMenuItem extends MenuItem {

		public SendMenuItem() {
			super("Send Message");
		}

		public void click() {
			Desktop.this.sendMessageUIAction.sendMessage();
		}
	}

	class ListMessagesMenuItem extends MenuItem {

		public ListMessagesMenuItem() {
			super("List Messages");
		}

		public void click() {
			Desktop.this.listMailboxUIAction.listMailbox();
		}
	}

	class DeleteMessageMenuItem extends MenuItem {

		public DeleteMessageMenuItem() {
			super("Delete Message");
		}

		public void click() {
			Desktop.this.deleteMessageUIAction.deleteMessage();
		}
	}

	class ExitMenuItem extends MenuItem {

		public ExitMenuItem() {
			super("Exit");
		}

		public void click() {
			application.stop();
		}
	}

	public void start() {
		Menu mainMenu = new Menu(new Console(), "Select an option:");

		mainMenu.addItem(new SendMenuItem());
		mainMenu.addItem(new DeleteMessageMenuItem());
		mainMenu.addItem(new ListMessagesMenuItem());
		mainMenu.addItem(new ExitMenuItem());

		while (true) {
			mainMenu.show();
		}
	}

}
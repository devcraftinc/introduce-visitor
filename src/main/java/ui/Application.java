package ui;

import domain.AddressBook;
import domain.Mailbox;

public class Application {

	private AddressBook addressBook = new AddressBook();

	public Application() {
	}

	public AddressBook getAddressBook() {
		return addressBook;
	}

	public void start() {

		Mailbox eranMbx = new Mailbox("Eran");
		Mailbox ronenMbx = new Mailbox("Dan");

		addressBook.addMailbox(eranMbx);
		addressBook.addMailbox(ronenMbx);

		new Desktop(this).start();
	}

	public void stop() {
		System.exit(0);
	}

	public static void main(String[] args) {
		new Application().start();
	}
}
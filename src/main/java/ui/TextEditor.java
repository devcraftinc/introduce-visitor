package ui;

import domain.AddressBook;
import domain.Text;

public class TextEditor extends java.lang.Object {

	private AddressBook addressBook;

	public TextEditor(AddressBook addressBook) {
		this.addressBook = addressBook;
	}

	public boolean edit(Text msg) {
		Console console = new Console();
		if (!MessageEditingUtils.editAddresses(msg, addressBook, console))
			return false;
		editContent(msg, console);
		return true;
	}

	private void editContent(Text msg, Console console) {
		String messageContent = console.readLine("Enter text:");
		if (!messageContent.trim().equals("")) {
			msg.setText(messageContent);
		}
	}
}